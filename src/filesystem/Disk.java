package filesystem;

import java.lang.*;
import java.io.*;
import java.io.File;
import java.io.IOException;

public class Disk {
    public RandomAccessFile disk;         // файл диска
    private int totalBlocks;
    private int blockSize;
    public static byte data[];
    File f;

    // ---------- Новый диск
    public Disk(String path) {
        totalBlocks = FileSystem.SIZE_FS / FileSystem.BLOCK_SIZE_BYTES;
        blockSize = FileSystem.BLOCK_SIZE_BYTES;
        data = new byte[totalBlocks * blockSize];
        try {
            // Создать файл диска
            f = new File(path);
            if(f.isFile())
                System.out.println("Файл диска уже существует");
            else {
                f.createNewFile();
                System.out.println("Создан новый файл диска: " + f.getAbsolutePath());
                Events.toFormat = 1;
            }
            disk = new RandomAccessFile(f, "rw");
            disk.read(data);
            disk.close();
        } catch (FileNotFoundException e) {
            System.out.println("Disk(): FileNotFoundException " + e);
            System.exit(1);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}