package filesystem;

import com.sun.xml.internal.bind.v2.runtime.output.ForkXmlOutput;
import com.sun.xml.internal.ws.util.ByteArrayBuffer;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.BitSet;

/**
 * Created by Svetlana on 20.11.2016.
 */

public class Superblock {
    int totalFreeInodes;
    public int totalFreeBlocks;
    int blockSize;      //размер блока
    int totalBlocks;       // размер ФС в блоках
    int totalInodes;       // количество инодов
    byte[] inodeEmptyList = new byte[65458];
    byte[] blockEmptyList = new byte[65458];

    int ilistOffset;
    int blocklistOffset;
    int rootOffset;
    public int DEOffset;

    //список адресов свободных блоков (со смещением)

    // -------- Задание размеров для ФС
    public Superblock(){}

    public Superblock(int i) {
        ByteArrayBuffer sblock = new ByteArrayBuffer();
        sblock.write(Disk.data, 0, 130952);
        byte[] superblock = sblock.getRawData();

        //считывание данных в переменные
        totalBlocks = ByteBuffer.wrap(superblock, 0, 4).getInt();
        totalFreeBlocks = ByteBuffer.wrap(superblock, 4, 4).getInt();
        blockSize = ByteBuffer.wrap(superblock, 8, 4).getInt();
        totalInodes = ByteBuffer.wrap(superblock, 12, 4).getInt();
        totalFreeInodes = ByteBuffer.wrap(superblock, 16, 4).getInt();

        ilistOffset = ByteBuffer.wrap(superblock, 20, 4).getInt();
        blocklistOffset = ByteBuffer.wrap(superblock, 24, 4).getInt();
        rootOffset = ByteBuffer.wrap(superblock, 28, 4).getInt();
        DEOffset = ByteBuffer.wrap(superblock, 32, 4).getInt();

        System.arraycopy(superblock, 36, inodeEmptyList, 0, 65458);      //список номеров свободных инодов
        System.arraycopy(superblock, 65494, blockEmptyList, 0, 65458);      //список номеров свободных инодов
    }

    //Установить инод как занятый
    public void setInodeBusy(int i) {
        inodeEmptyList[i] = 1;
    }

    public void setBlockBusy(int i) {
        blockEmptyList[i] = 1;
    }

    // получение СБ с байтовом виде
    public byte[] getBytes() throws IOException {
        ByteArrayBuffer superblock = new ByteArrayBuffer();

        // выяснение положения для записи информации из суперблока
        superblock.write(ByteBuffer.allocate(4).putInt(totalBlocks).array());
        superblock.write(ByteBuffer.allocate(4).putInt(totalFreeBlocks).array());
        superblock.write(ByteBuffer.allocate(4).putInt(blockSize).array());
        superblock.write(ByteBuffer.allocate(4).putInt(totalInodes).array());
        superblock.write(ByteBuffer.allocate(4).putInt(totalFreeInodes).array());

        superblock.write(ByteBuffer.allocate(4).putInt(ilistOffset).array());
        superblock.write(ByteBuffer.allocate(4).putInt(blocklistOffset).array());
        superblock.write(ByteBuffer.allocate(4).putInt(rootOffset).array());
        superblock.write(ByteBuffer.allocate(4).putInt(DEOffset).array());
        superblock.write(inodeEmptyList);
        superblock.write(blockEmptyList);

        byte[] rawData = superblock.getRawData();
        return rawData;
    }

    //--------------- Дать новому файлу/директории новый инод
    public short allocateInode() {
        short inode = -1;

        int i = totalInodes-totalFreeInodes;
        if(i > -1) {
            inodeEmptyList[i] = 1;
            inode = (short) i;
        }
        else {
            for (int ii = 0; ii < inodeEmptyList.length; ii++) {
                if (inodeEmptyList[ii] == 0) {
                    inode = (short) ii;
                    return inode;
                }
            }
        }
        return inode;
    }

    public int allocateBlock() {
        short block = -1;

        int i = totalBlocks-totalFreeBlocks;
        if(i > -1) {
            blockEmptyList[i] = 1;
            block = (short) i;
        }
        else {
            for (int ii = 0; ii < blockEmptyList.length; ii++) {
                if (blockEmptyList[ii] == 0) {
                    block = (short) ii;
                    return block;
                }
            }
        }
        return block;
    }
}
