package filesystem;

import com.sun.xml.internal.ws.util.ByteArrayBuffer;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;


public class Block {
    //public static final int BLOCK_OFFSET = 14353;       //в байтах; размер определён учитывая, что кол-во файлов = 25*25 и размер каждого = 21
    short state;        //1 - занят, 0 - свободен, -1 - конец файла
    Block nextBlock;    //    следующий блок
    int num;            //номер блока
    public static short blockSize = 10;       //колв-о байт на информацию о блоке

    public Block() {}
    public Block(int i) {
        num = i;
        state = 0;      //свободен по умолчанию
        nextBlock = new Block();
        nextBlock.num = -1;
    }

    public void getValues(byte[] buf) throws IOException {
        num = ByteBuffer.wrap(buf, 0, 4).getInt();
        state = ByteBuffer.wrap(buf, 4, 2).getShort();
        nextBlock.num = ByteBuffer.wrap(buf, 6, 4).getInt();
    }

    public void update(short s, int next) {
        state = s;
        nextBlock.num = next;
    }

    public byte[] getBytes() {
        ByteArrayBuffer buf = new ByteArrayBuffer();
        try {
            buf.write(ByteBuffer.allocate(4).putInt(num).array());
            buf.write(ByteBuffer.allocate(2).putShort(state).array());
            buf.write(ByteBuffer.allocate(4).putInt(nextBlock.num ).array());
            byte[] rawData = buf.getRawData();
            return rawData;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  null;
    }

    public void readValues(int offset) {
        RandomAccessFile file;
        try {
            file = new RandomAccessFile(Events.disk.f, "rw");
            file.seek(offset);

            ByteArrayBuffer buffer = new ByteArrayBuffer();
            while (buffer.size() != blockSize){
                buffer.write(file.readByte());
            }
            this.getValues(buffer.getRawData());
            file.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
