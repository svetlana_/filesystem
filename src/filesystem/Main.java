package filesystem;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import static filesystem.Events.*;

/**
 * Created by Svetlana on 22.11.2016.
 */
public class Main {
    public static void main ( String args[] ) throws IOException, NoSuchAlgorithmException {
        Events.boot();
        displayMenu();
    }
}
