package filesystem;

import com.sun.xml.internal.ws.util.ByteArrayBuffer;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class FileSystem {

    public static final int SIZE_FS = 32 * 1024 * 1024;        // кол-во блоков исходит из макс кол-ва файлов
    public static final int BLOCK_SIZE_BYTES = 1024;         //размер блока в байтах
    public static final int ILIST_OFFSET = 130988;


    private Superblock superblock;
    private Ilist ilist;
    private static BlockList blockList;
    public UserData user;
    public Group group;
    Login login;

    private DataElement curDataElement;       //содержит инод и имя папки В которой сейчас находимся
    private DataElement root;

    private byte[] dataElements;

    //-------- Создание виртуального диска
    public FileSystem() {
        superblock = new Superblock(SIZE_FS / BLOCK_SIZE_BYTES);
        root = new DataElement();
        getRootValues(false);
        curDataElement = root;
    }

    //считывание корневого каталога
    public void getRootValues(boolean b) {
        int offset = superblock.rootOffset;
        ByteArrayBuffer rootDir = new ByteArrayBuffer();
        rootDir.write(Disk.data, offset, 2048);         //считывание двух блоков, занятых под корневую директорию
        byte[] rootDirectory = rootDir.getRawData();
        root.name = "/".toCharArray();
        if (b)
            root.directDataElements.clear();
        for (int i = 21; i < rootDirectory.length - 21; i += 21) {        // длина массива - (макс.количество+информация о самом руте)
            ByteArrayBuffer temp = new ByteArrayBuffer();
            temp.write(rootDirectory, i, 21);               //выбор байт из root по смещению
            DataElement de = new DataElement();
            try {
                de.getValues(temp.getRawData());        //задание значений (№ инода и имя) для текущего элемента
                if (de.name[0] != Character.MIN_VALUE) {
                    root.directDataElements.add(de);
                    setAttributes(de);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void setAttributes(DataElement de) {
        Inode inode = new Inode();
        int inodeOffset = superblock.ilistOffset + de.iNum * Inode.inodeSize + de.iNum * 7;
        inode.readValues(inodeOffset);
        de.setInode(inode);
    }

    public void formatDisk() {
        superblock = new Superblock();
        superblock.totalBlocks = SIZE_FS / BLOCK_SIZE_BYTES;
        superblock.totalFreeBlocks = superblock.totalBlocks;
        superblock.blockSize = BLOCK_SIZE_BYTES;
        superblock.totalInodes = SIZE_FS / BLOCK_SIZE_BYTES;
        superblock.totalFreeInodes = superblock.totalInodes;

        superblock.ilistOffset = ILIST_OFFSET;       //начало записи ilist
        superblock.blocklistOffset = superblock.ilistOffset + (Inode.inodeSize * superblock.totalInodes);     //начало записи blocklist
        superblock.rootOffset = superblock.blocklistOffset + (Block.blockSize * superblock.totalBlocks);      //начало записи рута (на два блока)
        superblock.DEOffset = superblock.rootOffset + 2048;     //конец записи рута

        root = new DataElement();
        root.name = "/".toCharArray();
        getRootValues(true);
        Inode rootInode = new Inode(0);
        rootInode.type_p = false;      // по умолчанию - не директория, а файл
        rootInode.UID = 0;            //0 - админ
        rootInode.GID = 0;            //0 - админ
        rootInode.access = 770;       //7 - полный доступ
        rootInode.fileSize = superblock.blockSize * 2;
        rootInode.blockNum = -1;      // нет блока-ссылки на этот инод

        rootInode.createDate = DateTimeFormatter.ofPattern("MM/dd/yyyy").format(LocalDate.now());
        rootInode.updateDate = DateTimeFormatter.ofPattern("MM/dd/yyyy").format(LocalDate.now());

        try {
            ilist = new Ilist(superblock.totalInodes);
            ilist.inodes[0] = rootInode;
            root.setInode(rootInode);
            superblock.inodeEmptyList[0] = 1;
            superblock.totalFreeInodes--;
            blockList = new BlockList(superblock.totalBlocks);        //список блоков
        } catch (IOException e) {
            e.printStackTrace();
        }
        dataElements = new byte[SIZE_FS - superblock.DEOffset];
        int countBusyBlocks = superblock.DEOffset / superblock.blockSize + 1;      //количество блоков под системную область
        for (int i = 0; i <= countBusyBlocks; i++)
            superblock.setBlockBusy(i);
        superblock.totalFreeBlocks -= countBusyBlocks;
        System.out.println(superblock.totalBlocks);
    }

    //вызывается при создании нового файла, записи в него, удалении
    public void formatWrite() {
        try {
            RandomAccessFile file = new RandomAccessFile(Events.disk.f, "rw");
            file.write(superblock.getBytes());
            file.seek(superblock.ilistOffset);
            file.write(ilist.getBytes());
            file.seek(superblock.blocklistOffset);
            file.write(blockList.getBytes());
            file.seek(superblock.rootOffset);
            file.write(root.getBytes());
            file.write(dataElements);
            Events.disk.disk = new RandomAccessFile(Events.disk.f, "rw");
            Events.disk.disk.read(Events.disk.data);
            file.close();
            SBUpdate();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void createDataElement(String fullName) {
        DataElement backup = curDataElement;
        boolean type = !(fullName.endsWith("/"));       // является файл директорией или файлом
        getRootValues(true);        //обновление информации из рута
        curDataElement = new DataElement();
        curDataElement.name = fullName.toCharArray();

        short iNum = superblock.allocateInode();     //назначение файлу (директории) нового свободного инода
        Inode inode = new Inode(iNum);
        if (iNum == -1) {
            Events.cerr("Нет свободных инодов");
            return;
        }
        int inodeAddr = superblock.ilistOffset + iNum * Inode.inodeSize+iNum*7;        //адрес нужного инода на диске
        inode.readValues(inodeAddr);
        inode.update(-1, type, 0, user.UID, user.GID, (short)777);

        int acc = login.checkAccess(inode, user);
        if (!(acc == 5 || acc == 7 || acc == 3 || acc == 1)) {
            Events.cerr("Недостаточно прав для удаления!");
            return;
        }

        curDataElement.setInode(inode);         //по информации в иноде обновление информации о файле
        superblock.totalFreeInodes--;
        superblock.setInodeBusy(iNum);
        ////////////////////

        int DEAddr = superblock.rootOffset + iNum * DataElement.dataElSize;     //адрес информации о нужном файле на диске

        writeDataElementToDisk(DEAddr, inodeAddr, curDataElement, inode);
        root.directDataElements.add(curDataElement);
        curDataElement = backup;

        System.out.println("Файл создан");
    }

    public void writeDataElementToDisk(int DEAddr, int inodeAddr, DataElement tempDE, Inode inode) {
        try {
            RandomAccessFile file = new RandomAccessFile(Events.disk.f, "rw");
            file.seek(inodeAddr);
            file.write(inode.getBytes());
            file.seek(DEAddr);
            file.write(tempDE.getBytes());
            SBUpdate();
            Events.disk.disk = new RandomAccessFile(Events.disk.f, "rw");
            Events.disk.disk.read(Events.disk.data);
            Events.disk.disk.close();
            file.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Перемещение по ФС (команда cd)
    public DataElement setCurDir(String fullName) {
        getRootValues(true);        //обновить информацию в руте
        if(fullName.trim().equals(new String(root.name).trim()))
            curDataElement = root;
        else curDataElement = root.seekDir(fullName);
        return curDataElement;
    }

    public String getCurDirName() {
        byte[] nname = curDataElement.getName();
        return new String(nname).trim();
    }

    public void showDirContent(boolean full) {
        getRootValues(true);
        if(!(new String (curDataElement.name).trim().equals(new String(root.name).trim())))
            curDataElement.getDirContent(root);
        else curDataElement = root;
        System.out.println("Права" + "\t" + "UID" + "\t" + "Размер" + "\t" + "Дата изм." + "\t" + "Имя");
        for (int i = 0; i < curDataElement.directDataElements.size(); i++) {
            byte[] nname = curDataElement.directDataElements.get(i).getName();
            if(!full)
                System.out.println(new String(nname).toCharArray());
            else {
                String access, UID, fileSize, name, updDate;
                name = new String(nname);
                access = Short.toString(curDataElement.directDataElements.get(i).access);
                UID = Short.toString(curDataElement.directDataElements.get(i).UID);
                fileSize = Integer.toString(curDataElement.directDataElements.get(i).fileSize);
                updDate = curDataElement.directDataElements.get(i).updateDate;
                System.out.println(access + "\t" + "\t" + UID + "\t" + fileSize + "\t" + "\t" + updDate  + "\t" +  name);
            }
        }
    }

    public void SBUpdate() {
        RandomAccessFile file = null;
        try {
            file = new RandomAccessFile(Events.disk.f, "rw");
            file.write(superblock.getBytes());
            Events.disk.disk = new RandomAccessFile(Events.disk.f, "rw");
            Events.disk.disk.read(Events.disk.data);
            file.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void writeToFile(String fileName, byte[] data) {
        //поиск файла
        DataElement de = root.seekDir(fileName);
        if (de == null)
            Events.cerr("Файл не найден");

        Inode inode = new Inode();
        int inodeAddr = superblock.ilistOffset + de.iNum * Inode.inodeSize + de.iNum * 7;        //адрес нужного инода на диске
        inode.readValues(inodeAddr);

        int acc = login.checkAccess(inode, user);
        if (!(acc == 6 || acc == 7 || acc == 3 || acc == 2)) {
            Events.cerr("Недостаточно прав для записи!");
            return;
        }

        int blockCount = data.length / superblock.blockSize + 1;
        if (blockCount > 10 || superblock.totalFreeBlocks < blockCount) {
            Events.cerr("Слишком много данных для записи");
            return;
        }
        //////////////Поиск блоков////////////////
        Block[] blocksToWrite = new Block[10];   //объекты блоков
        int[] fileSizeBlocks = new int[10];     //номера блоков
        for (int i = 0; i < blockCount; i++) {
            fileSizeBlocks[i] = superblock.allocateBlock();
            blocksToWrite[i] = new Block(fileSizeBlocks[i]);
        }
        for (int i = 0; i < blockCount - 1; i++) {
            blocksToWrite[i].update((short) 1, fileSizeBlocks[i + 1]);         //обновление информации в блоке
            superblock.blockEmptyList[fileSizeBlocks[i]] = 1;
            superblock.totalFreeBlocks--;
        }
        blocksToWrite[blockCount - 1].update((short) 1, -1);
        ///////////////Запись файлов в блоки исходя из их размера///////////////
        byte[][] fileData = new byte[blockCount][];
        for (int i = 0, j = 0; i < data.length; i += superblock.blockSize, j++) {
            int size = (data.length - i < superblock.blockSize) ? data.length - i : superblock.blockSize;
            byte[] tmp = new byte[superblock.blockSize];
            System.arraycopy(data, i, tmp, 0, size);
            fileData[j] = tmp;
        }
        try {
            RandomAccessFile file = new RandomAccessFile(Events.disk.f, "rw");
            inode.update(fileSizeBlocks[0], true, data.length, user.UID, user.GID, (short)777);
            file.seek(inodeAddr);
            file.write(inode.getBytes());
            for (int i = 0; i < blockCount; i++) {
                int blockAddr = superblock.blocklistOffset + fileSizeBlocks[i] * superblock.blockSize;

                file.seek(blockAddr);
                file.write(blocksToWrite[i].getBytes());
            }

            for (int i = 0; i < blockCount; i++) {
                int offset = superblock.DEOffset + fileSizeBlocks[i] * superblock.blockSize;
                file.seek(offset);
                file.write(fileData[i]);
            }
            Events.disk.disk = new RandomAccessFile(Events.disk.f, "rw");
            Events.disk.disk.read(Events.disk.data);
            Events.disk.disk.close();
            file.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public byte[] readFromFile(String fileName) {
        //поиск файла
        DataElement de = root.seekDir(fileName);
        if (de == null)
            Events.cerr("Файл не найден");

        Inode inode = new Inode();
        int inodeAddr = superblock.ilistOffset + de.iNum * Inode.inodeSize + de.iNum * 7;        //адрес нужного инода на диске
        inode.readValues(inodeAddr);

        int acc = login.checkAccess(inode, user);
        if (!(acc == 6 || acc == 7 || acc == 4 || acc == 5)) {
            Events.cerr("Недостаточно прав для чтения!");
            return null;
        }

        Block[] blocksToRead = new Block[10];
        int blockCount = 0;

        int blockAddr = superblock.blocklistOffset + inode.blockNum * superblock.blockSize;
        for (int i = 0; ; i++) {
            blocksToRead[i] = new Block(i);
            blocksToRead[i].readValues(blockAddr);
            blockCount++;
            if (blocksToRead[i].nextBlock.num == -1) break;
            blockAddr = superblock.blocklistOffset + blocksToRead[i].nextBlock.num * superblock.blockSize;
        }

        ByteArrayBuffer data = new ByteArrayBuffer();
        RandomAccessFile f = null;
        try {
            f = new RandomAccessFile(Events.disk.f, "rw");
            for (int i = 0; i < blockCount; i++) {
                int offset = superblock.DEOffset + blocksToRead[i].num * superblock.blockSize;
                f.seek(offset);
                ByteArrayBuffer tmp = new ByteArrayBuffer();
                while (tmp.size() != superblock.blockSize) {
                    tmp.write(f.readByte());
                }
                data.write(tmp.getRawData());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return data.getRawData();
    }

    public void delete(String fullname) {
        DataElement de = root.seekDir(fullname.trim());
        if (de == null) {
            Events.cerr("Файл не найден");
            return;
        }

        Inode inode = new Inode();
        int inodeAddr = superblock.ilistOffset + de.iNum * Inode.inodeSize + de.iNum*7;        //адрес нужного инода на диске
        inode.readValues(inodeAddr);

        int acc = login.checkAccess(inode, user);
        if (!(acc == 5 || acc == 7 || acc == 3 || acc == 1)) {
            Events.cerr("Недостаточно прав для удаления!");
            return;
        }

        Block[] blocksToDelete = new Block[10];
        int blockCount = 0;

        int[] fileSizeBlocks = null;
        int blockAddr = 0;
        if (inode.blockNum != -1) {      //если файл занимает какие-то блоки, то обнуляем их до дефолта
            fileSizeBlocks = new int[10];     //номера блоков
            blockAddr = superblock.blocklistOffset + inode.blockNum * superblock.blockSize;
            for (int i = 0; ; i++) {
                blocksToDelete[i] = new Block(i);
                blocksToDelete[i].readValues(blockAddr);
                fileSizeBlocks[i] = blocksToDelete[i].num;
                blockCount++;
                if (blocksToDelete[i].nextBlock.num < 0) break;
                blockAddr = superblock.blocklistOffset + blocksToDelete[i].nextBlock.num * superblock.blockSize;
            }

            //////////////Поиск блоков и освобождение информации////////////////
            for (int i = 0; i < blockCount - 1; i++) {
                blocksToDelete[i].update((short) 0, -1);         //обновление информации в блоке
                superblock.blockEmptyList[fileSizeBlocks[i]] = 0;
                superblock.totalFreeBlocks++;
            }
            blocksToDelete[blockCount - 1].update((short) 0, -1);
        }

        if(fullname.endsWith("/")) {        //если удаляемый файл - директория, то удаляем всё её содержимое (если есть права)
            getRootValues(true);
            de.getDirContent(root);
            for(DataElement ddd : de.directDataElements) {
                delete(new String(ddd.name));
            }
        }

        try {
            RandomAccessFile file = new RandomAccessFile(Events.disk.f, "rw");
            if (inode.blockNum != -1) {
                for (int i = 0; i < blockCount; i++) {
                    blockAddr = superblock.blocklistOffset + fileSizeBlocks[i] * superblock.blockSize;

                    file.seek(blockAddr);
                    file.write(blocksToDelete[i].getBytes());
                }
            }

            inode.update(-1, true, 0, (short) 0, (short) 0, (short)775);
            file.seek(inodeAddr);
            file.write(inode.getBytes());
            superblock.inodeEmptyList[inode.num] = 0;
            superblock.totalFreeInodes++;
            int DEAddr = superblock.rootOffset + de.iNum * DataElement.dataElSize;
            de.iNum = -1;
            de.name = "".toCharArray();
            file.seek(DEAddr);
            file.write(de.getBytes());

            Events.disk.disk = new RandomAccessFile(Events.disk.f, "rw");
            Events.disk.disk.read(Events.disk.data);
            Events.disk.disk.close();
            file.close();
            SBUpdate();
            getRootValues(true);
            root.directDataElements.remove(de);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isExist(String fullName) {
        DataElement de = root.seekDir(fullName);
        return de != null;
    }

    public void move(String fullNameSource, String fullNameDestination, String shortNameSource) {
        getRootValues(true);
        DataElement deSource = root.seekDir(fullNameSource);
        root.directDataElements.remove(deSource);       //удаление из текущего местоположения
        DataElement deDest = root.seekDir(fullNameDestination);
        rename(deSource,deDest,fullNameDestination,shortNameSource);

        boolean type = !(fullNameSource.endsWith("/"));
        if(!type) {     //если перемещаем директорию
            DataElement deParent = new DataElement();
            deParent.name = fullNameSource.toCharArray();
            deDest = deSource;      //т.к. папка уже перемещена, то файлы идут уже в неё
            deParent.getDirContent(root);
            for(DataElement d : deParent.directDataElements) {
                root.directDataElements.remove(d);
                String[] tokens = new String(d.name).split("[/]");
                shortNameSource = tokens[tokens.length-1].trim();
                rename(d,deDest,new String(deDest.name),shortNameSource);
            }
        }
    }

    private void rename(DataElement deSource, DataElement deDest, String fullNameDestination, String shortNameSource) {
        deSource.name = (fullNameDestination + shortNameSource).toCharArray();

        ///////Обновление размера в иноде папки/////
        RandomAccessFile file;
        try {
            file = new RandomAccessFile(Events.disk.f, "rw");

            //получение размера исходного файла
            int fSizeSourceOffset = superblock.ilistOffset + deSource.iNum * Inode.inodeSize + deSource.iNum * 7 + 47;   //47 - начало int размера
            file.seek(fSizeSourceOffset);
            ByteArrayBuffer buffer = new ByteArrayBuffer();
            while (buffer.size() != 10) {     //считывание размера
                buffer.write(file.readByte());
            }
            int fSize = ByteBuffer.wrap(buffer.getRawData(), 0, 4).getInt();
            //int accS = ByteBuffer.wrap(buffer.getRawData(), 8, 2).getShort();

            Inode inodeDest = new Inode();
            int inodeDestOffset = superblock.ilistOffset + deDest.iNum * Inode.inodeSize + deDest.iNum * 7;
            inodeDest.readValues(inodeDestOffset);
            inodeDest.fileSize+=fSize;

            int accD = login.checkAccess(inodeDest, user);
            if (!(accD == 7 || accD == 5 || accD == 3 || accD == 1)) {
                Events.cerr("Недостаточно прав для выполнения действия!");
                return;
            }

            file.seek(inodeDestOffset);
            file.write(inodeDest.getBytes());      //изменение размера папки на диске

            int DEAddr = superblock.rootOffset + deSource.iNum * DataElement.dataElSize;    //адрес файла в руте
            file.seek(DEAddr);
            file.write(deSource.getBytes());    //Изменение имени файла в руте
            Events.disk.disk = new RandomAccessFile(Events.disk.f, "rw");
            Events.disk.disk.read(Events.disk.data);
            Events.disk.disk.close();
            file.close();
            SBUpdate();
            //getRootValues(true);
            root.directDataElements.add(deSource);
            Events.cerr("Файл перемещён");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void chmod(String fileName, short newAccess) {
        DataElement de = root.seekDir(fileName);

        Inode inode = new Inode();
        int inodeAddr = superblock.ilistOffset + de.iNum * Inode.inodeSize + de.iNum * 7;
        inode.readValues(inodeAddr);

        inode.access = newAccess;
        try {
            RandomAccessFile file = new RandomAccessFile(Events.disk.f, "rw");
            file.seek(inodeAddr);
            file.write(inode.getBytes());
            SBUpdate();
            Events.disk.disk = new RandomAccessFile(Events.disk.f, "rw");
            Events.disk.disk.read(Events.disk.data);
            Events.disk.disk.close();
            file.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
