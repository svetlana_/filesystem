package filesystem;

import com.sun.xml.internal.ws.util.ByteArrayBuffer;
import java.io.*;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Inode {
    //---------- Информация, записываемая на диск
    boolean type_p;     // тип файла, true -> file, false -> directory
    int num;            //№ инода
    int blockNum;       //№ первого блока, в который записывается файл
    String createDate;
    String updateDate;
    short UID, GID;
    short access;
    int fileSize;           //размер файла в байтах
    public static final int inodeSize = 57;     //размер инода

    public Inode() {}

    public Inode(int _num) {
        num = _num;     // номер данного инода
        type_p = true;      // по умолчанию - файл
        UID = 0;
        GID = 0;
        access = 777;
        fileSize = 0;
        blockNum = -1;      // нет блока-ссылки на этот инод

        createDate = DateTimeFormatter.ofPattern("MM/dd/yyyy").format(LocalDate.now());
        updateDate = DateTimeFormatter.ofPattern("MM/dd/yyyy").format(LocalDate.now());
    }

    public byte[] getBytes()  {
        ByteArrayBuffer buf = new ByteArrayBuffer();
        byte tmp = (byte) (type_p ? 1 : 0);
        try {
            buf.write(ByteBuffer.allocate(1).put(tmp).array());
            buf.write(ByteBuffer.allocate(4).putInt(num).array());
            buf.write(createDate.getBytes());
            buf.write(updateDate.getBytes());
            buf.write(ByteBuffer.allocate(4).putInt(blockNum).array());
            buf.write(ByteBuffer.allocate(4).putInt(fileSize).array());
            buf.write(ByteBuffer.allocate(2).putShort(UID).array());
            buf.write(ByteBuffer.allocate(2).putShort(GID).array());
            buf.write(ByteBuffer.allocate(2).putShort(access).array());
            byte[] rawData = buf.getRawData();
            return rawData;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  null;
    }

    public void getValues(byte[] buf) throws IOException {
        byte[] arr = new byte[inodeSize];
        System.arraycopy(buf, 1, arr, 1, 4);
        byte tmp = buf[0];
        type_p = !(tmp == 0);
        num = ByteBuffer.wrap(buf, 1, 4).getInt();
        byte[] date = new byte[10];
        System.arraycopy(buf, 5, date, 0, 10);
        createDate = new String(date);
        System.arraycopy(buf, 15, date, 0, 10);
        updateDate = new String(date);
        blockNum = ByteBuffer.wrap(buf, 25, 4).getInt();
        fileSize = ByteBuffer.wrap(buf, 29, 4).getInt();
        UID = ByteBuffer.wrap(buf, 33, 2).getShort();
        GID = ByteBuffer.wrap(buf, 35, 2).getShort();
        access = ByteBuffer.wrap(buf, 37, 2).getShort();
    }

    // Обновлять информацию в иноде при создании файла/директории
    public void update(int bNum, boolean type, int _fileSize, short _UID, short _GID, short _access) {
        blockNum = bNum;        //определение первого свободного блока
        updateDate = DateTimeFormatter.ofPattern("MM/dd/yyyy").format(LocalDate.now());
        fileSize = _fileSize;
        type_p = type;
        UID = _UID;
        GID = _GID;
        access = _access;
    }

    public void readValues(int offset) {
        RandomAccessFile file;
        try {
            file = new RandomAccessFile(Events.disk.f,"rw");
            file.seek(offset);

        ByteArrayBuffer buffer = new ByteArrayBuffer();
        while (buffer.size() != inodeSize){
            buffer.write(file.readByte());
        }
        this.getValues(buffer.getRawData());
        file.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}