package filesystem;

import com.sun.xml.internal.ws.util.ByteArrayBuffer;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;

public class DataElement {

    char[] extention = new char[5];
    public char[] name = new char[19];
    public short iNum;

    boolean type_p;     // тип файла, true -> file, false -> directory
    int blockNum;       //№ первого блока, в который записывается файл
    String createDate, updateDate;
    short UID, GID;
    short access;
    int fileSize;           //размер файла в байтах

    public static short dataElSize = 21;      //байтовый размер одной записи
    ArrayList<DataElement> directDataElements;

    public DataElement() {
        directDataElements = new ArrayList<>();
        iNum = -1;
        name = "".toCharArray();
        extention = null;
    }

    //установить информацию из инода для отображения пользователю
    public void setInode(Inode inode) {
        iNum = (short)inode.num;
        //extention = de.name.split([.]);
        type_p = inode.type_p;
        blockNum = inode.blockNum;
        createDate = inode.createDate;
        updateDate = inode.updateDate;
        UID = inode.UID;
        GID = inode.GID;
        access = inode.access;
        fileSize = inode.fileSize;
    }

    //Получить данные для записи в рут
    public byte[] getBytes() {
        byte[] arr = new byte[21];
        String fullname = new String(name).trim();
        int max = fullname.length() < 19 ? fullname.length() : 19;
        byte[] i = ByteBuffer.allocate(2).putShort(iNum).array();
        arr[0] = i[0];
        arr[1] = i[1];
        for (int j = 0; j < max; j++) {
            arr[j+2] = (byte)fullname.toCharArray()[j];
        }
        return arr;
    }

    //Получение данных из рута для определённой записи
    public void getValues(byte[] buf) throws IOException {
        this.iNum = (short)(buf[0] << 8 | buf[1] & 0xFF);
        byte[] tmp = new byte[19];
        System.arraycopy(buf, 2, tmp, 0, 19);
        name = new String(tmp).toCharArray();
    }

    public DataElement seekDir(String fullName) {
        DataElement de = null;
        for(DataElement d : directDataElements){
            if(new String(d.name).trim().equals(fullName))
                de = d;
        }
        return de;
    }

    public byte[] getName() {
        byte[] arr = new byte[19];
        String fullname = new String(name).trim();
        int max = fullname.length() < 19 ? fullname.length() : 19;
        for (int j = 0; j < max; j++) {
            arr[j] = (byte)fullname.toCharArray()[j];
        }
        return arr;
    }

    public void getDirContent(DataElement root) {
        for(DataElement d : root.directDataElements){
            String[] tokens = (new String(d.name)).trim().split("[/]");
            String n = new String(name).trim();
            String[] nsplit = new String(name).trim().split("[/]");
            if(new String(d.name).trim().contains(new String(name).trim()) && !(nsplit[1].equals(tokens[tokens.length-1]) )) {
                this.directDataElements.add(d);
            }
        }
    }
}
