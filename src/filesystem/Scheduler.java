package filesystem;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Random;

import static filesystem.Events.inp;

/**
 * Существует две разновидности приоритетного планирования: с относительными и абсолютными приоритетами.
 * В обоих случаях на выполнение выбирается поток, имеющий наивысший приоритет.
 * В системах с относительными приоритетами активный поток выполняется до тех пор, пока он сам не покинет процессор,
 * перейдя в состояние ожидания (по вводу-выводу, например), или не завершится, или не произойдет ошибка.
 * В системах с абсолютными приоритетами выполнение активного потока прерывается еще и по причине появления потока,
 * имеющего более высокий приоритет, чем у активного потока. В этом случае прерванный поток переходит в состояние готовности.
 */
class Process {
    public enum Status {Ready, Working, Suspended, Complete}

    public int numList;
    public String name;
    public int time;
    public Status status;
    public int PID;
    public int priority;

    public Process(int numList, String name, int time, Status stat, int PID, int pr) {
        this.numList = numList;
        this.name = name;
        this.time = time;
        this.status = stat;
        this.PID = PID;
        this.priority = pr;
    }
}

public class Scheduler {
    static  ArrayList<Process> list1 = new ArrayList<>();       //абсолютные
    static ArrayList<Process> list2 = new ArrayList<>();
    static ArrayList<Process> list3 = new ArrayList<>();
    static ArrayList<Process> list4 = new ArrayList<>();       //относительные
    static ArrayList<Process> result = new ArrayList<>();
    ArrayList<ArrayList<Process>> all = new ArrayList<>();

    static int quantum = 25;

    static Process curProc;

    int step = 0;

    //заполнение списков процессов
    public void init() {
        list1.add(new Process(1, "pr01", 50, Process.Status.Ready, 542, 6));
        list1.add(new Process(1, "pr02", 22, Process.Status.Ready, 657, 0));
        /*list1.add(new Process(1, "pr03", 26, Process.Status.Ready, 758, 1));
        list1.add(new Process(1, "pr04", 80, Process.Status.Ready, 111, 3));

        list2.add(new Process(2, "pr05", 30, Process.Status.Ready, 752, 15));
        list2.add(new Process(2, "pr06", 10, Process.Status.Ready, 441, 12));*/
        list2.add(new Process(2, "pr07", 15, Process.Status.Ready, 666, 11));
        list2.add(new Process(2, "pr08", 49, Process.Status.Ready, 102, 10));

        list3.add(new Process(3, "pr09", 45, Process.Status.Ready, 785, 28));
        list3.add(new Process(3, "pr10", 10, Process.Status.Ready, 963, 21));
        /*list3.add(new Process(3, "pr11", 22, Process.Status.Ready, 901, 29));
        list3.add(new Process(3, "pr12", 10, Process.Status.Ready, 10, 21));

        list4.add(new Process(4, "pr13", 10, Process.Status.Ready, 110, 32));*/
        list4.add(new Process(4, "pr14", 12, Process.Status.Ready, 444, 34));
        list4.add(new Process(4, "pr15", 52, Process.Status.Ready, 222, 33));
        list4.add(new Process(4, "pr16", 28, Process.Status.Ready, 365, 31));
    }

    private void setComplete() {
        try {
            if (curProc.time < 1) {
                curProc.status = Process.Status.Complete;
                curProc.numList = 0;
                result.add(curProc);
            }
        } catch (Exception e) { }
    }

    public void print() {

        all.clear();
        all.add(list1);
        all.add(list2);
        all.add(list3);
        all.add(list4);

        setComplete();      //возможное изменение статуса процесса

        System.out.println("Выполняемый процесс:");
        System.out.println("Шаг № " + step);
        step++;
        FileWriter writer = null;

        try {
            System.out.println("Выполняемый процесс:");
            System.out.println(curProc.name + "\t" + curProc.PID + "\t" + curProc.priority + "\t" + curProc.status + "\t" + curProc.time);
        } catch (Exception eeeee) {
            Events.cerr("---");
        }

        try {
            writer = new FileWriter("procs.txt", true);

            System.out.println("NAME" + "\t" + "PID" + "\t" + "PR" + "\t" + "STAT" + "\t" + "TIME");
            writer.append(System.getProperty("line.separator"));
            writer.write("NAME" + "\t" + "PID" + "\t" + "PR" + "\t" + "STAT" + "\t" + "TIME");
            System.out.println("Очередь 1:");
            writer.append(System.getProperty("line.separator"));
            writer.write("Очередь 1:");
            for (Process t : list1) {
                System.out.println(t.name + "\t" + t.PID + "\t" + t.priority + "\t" + t.status + "\t" + t.time);
                writer.append(System.getProperty("line.separator"));
                writer.write(t.name + "\t" + t.PID + "\t" + t.priority + "\t" + t.status + "\t" + t.time);
            }
            System.out.println("Очередь 2:");
            writer.append(System.getProperty("line.separator"));
            writer.write("Очередь 2:");
            for (Process t : list2) {
                System.out.println(t.name + "\t" + t.PID + "\t" + t.priority + "\t" + t.status + "\t" + t.time);
                writer.append(System.getProperty("line.separator"));
                writer.write(t.name + "\t" + t.PID + "\t" + t.priority + "\t" + t.status + "\t" + t.time);
            }
            System.out.println("Очередь 3:");
            writer.append(System.getProperty("line.separator"));
            writer.write("Очередь 3:");
            for (Process t : list3) {
                System.out.println(t.name + "\t" + t.PID + "\t" + t.priority + "\t" + t.status + "\t" + t.time);
                writer.append(System.getProperty("line.separator"));
                writer.write(t.name + "\t" + t.PID + "\t" + t.priority + "\t" + t.status + "\t" + t.time);
                }
            System.out.println("Очередь 4:");
            writer.append(System.getProperty("line.separator"));
            writer.write("Очередь 4:");
            for (Process t : list4) {
                System.out.println(t.name + "\t" + t.PID + "\t" + t.priority + "\t" + t.status + "\t" + t.time);
                writer.append(System.getProperty("line.separator"));
                writer.write(t.name + "\t" + t.PID + "\t" + t.priority + "\t" + t.status + "\t" + t.time);
            }
            System.out.println("Завершённые:");
            for (Process t : result)
                System.out.println(t.name + "\t" + t.PID + "\t" + t.priority + "\t" + t.status + "\t" + t.time);
            System.out.println("____________________________");

            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void run() {
        if (list1.size() != 0 || list2.size() != 0 || list3.size() != 0 || list4.size() != 0) {

            ////////Выполнение процессов в 1 очереди//////////
            try {
                if (list1.size() != 0 || curProc.numList == 1) {
                    if (curProc.numList != 1) {
                        if (curProc.numList == 2) {
                            curProc.status = Process.Status.Suspended;
                            list2.add(curProc);
                        }

                        if (curProc.numList == 3) {
                            curProc.numList--;
                            curProc.status = Process.Status.Suspended;
                            curProc.priority -= 10;
                            list2.add(curProc);
                        }

                        if (curProc.numList == 4) {
                            curProc.status = Process.Status.Suspended;
                            list4.add(curProc);
                        }
                    }
                    if (curProc.status != Process.Status.Complete && curProc.numList == 1) {
                        list1.add(curProc);
                        curProc.status = Process.Status.Ready;
                    }
                    for (int i = 0; i < 10; i++)
                        for (Process t : list1)
                            if (t.priority == i) {
                                curProc = t;
                                list1.remove(curProc);
                                curProc.status = Process.Status.Working;
                                i = 10;
                                break;
                            }
                    nextStep();
                    return;
                }
            } catch (Exception e) {
                for (int i = 0; i < 10; i++)
                    for (Process t : list1)
                        if (t.priority == i) {
                            curProc = t;
                            list1.remove(curProc);
                            curProc.status = Process.Status.Working;
                            i = 10;
                            break;
                        }
                if(curProc != null) {
                    nextStep();
                    return;
                }
            }

            //////Выполнение процессов во 2 очереди////////
            try {
                if (list2.size() != 0 || curProc.numList == 2) {
                    if (curProc.status != Process.Status.Complete) {
                        list2.add(curProc);
                        curProc.status = Process.Status.Ready;
                    }
                    int i = 0;
                    for (Process t : list2) {
                        if (t.status == Process.Status.Suspended) {
                            curProc = t;
                            list2.remove(curProc);
                            curProc.status = Process.Status.Working;
                            i = 20;
                            break;
                        }
                    }
                    if (i != 20)
                        for (i = 10; i < 20; i++)
                            for (Process t : list2) {
                                if (t.priority == i) {
                                    curProc = t;
                                    list2.remove(curProc);
                                    curProc.status = Process.Status.Working;
                                    i = 20;
                                    break;
                                }
                            }
                    nextStep();
                    return;
                }
            } catch (Exception e) {
                for (int i = 10; i < 20; i++)
                    for (Process t : list2) {
                        if (t.status == Process.Status.Suspended) {
                            curProc = t;
                            list2.remove(curProc);
                            curProc.status = Process.Status.Working;
                            i = 20;
                            break;
                        }
                        if (t.priority == i) {
                            curProc = t;
                            list2.remove(curProc);
                            curProc.status = Process.Status.Working;
                            i = 20;
                            break;
                        }
                    }
                if(curProc != null) {
                    nextStep();
                    return;
                }
            }

            //////Выполенение 3 очереди//////////
            try {
                if (list3.size() != 0 || curProc.numList == 3) {
                    if (curProc.status != Process.Status.Complete) {
                        list3.add(curProc);
                        curProc.status = Process.Status.Ready;
                    }
                    for (int i = 20; i < 30; i++)
                        for (Process t : list3)
                            if (t.priority == i) {
                                curProc = t;
                                list3.remove(curProc);
                                curProc.status = Process.Status.Working;
                                i = 30;
                                break;
                            }
                    nextStep();
                    return;
                }
            } catch (Exception e) {
                for (int i = 20; i < 30; i++)
                    for (Process t : list3)
                        if (t.priority == i) {
                            curProc = t;
                            list3.remove(curProc);
                            curProc.status = Process.Status.Working;
                            i = 30;
                            break;
                        }
                if(curProc != null) {
                    nextStep();
                    return;
                }
            }

            try {
                if (list4.size() != 0)
                    for (int i = 30; i < 40; i++)
                        for (Process t : list4) {
                            if (t.status == Process.Status.Suspended) {
                                curProc = t;
                                list4.remove(curProc);
                                curProc.status = Process.Status.Working;
                                i = 40;
                                break;
                            }
                            if (t.priority == i) {
                                curProc = t;
                                list4.remove(curProc);
                                curProc.status = Process.Status.Working;
                                i = 40;
                                break;
                            }
                        }
            } catch (Exception e) {
                for (int i = 30; i < 40; i++)
                    for (Process t : list4) {
                        if (t.status == Process.Status.Suspended) {
                            curProc = t;
                            list4.remove(curProc);
                            curProc.status = Process.Status.Working;
                            i = 40;
                            break;
                        }
                        if (t.priority == i) {
                            curProc = t;
                            list4.remove(curProc);
                            curProc.status = Process.Status.Working;
                            i = 40;
                            break;
                        }
                    }
            }
            if(curProc != null)
                nextStep();
        } else Events.cerr("Нет процессов на выполнение");
    }

    private void nextStep() {
            if (curProc.numList == 4)
            curProc.time -= curProc.time;
        else {
            if (curProc.time >= quantum)
                curProc.time -= quantum;
            else
                curProc.time -= curProc.time;
        }

        for (Process i : list2)
            if (i.priority != 10)
                i.priority--;

        ArrayList<Integer> tmp = new ArrayList<>();
        tmp.clear();
        for (Process i : list3) {
            if (i.priority != 20)
                i.priority--;
            else {
                i.numList--;
                list2.add(i);
                tmp.add(list3.indexOf(i));
            }
        }
        for (int i = 0; i < tmp.size(); i++)
            list3.remove(tmp.get(i) - i);

        tmp.clear();
        for (Process i : list4) {
            if (i.priority != 30)
                i.priority--;
            else {
                i.numList--;
                list3.add(i);
                tmp.add(list4.indexOf(i));
            }
        }
        for (int i = 0; i < tmp.size(); i++)
            list4.remove(tmp.get(i) - i);
    }

    public void addProc(String name, int time, int pr) {
        Random r = new Random();
        int pid = r.nextInt(1000);
        if (pr < 10 && pr > -1)
            list1.add(new Process(1, name, time, Process.Status.Ready, pid, pr));
        if (pr < 20 && pr > 9)
            list2.add(new Process(2, name, time, Process.Status.Ready, pid, pr));
        if (pr < 30 && pr > 19)
            list3.add(new Process(3, name, time, Process.Status.Ready, pid, pr));
        if (pr < 40 && pr > 29)
            list4.add(new Process(4, name, time, Process.Status.Ready, pid, pr));
    }

    public void changePriority(int pid, int pr) {
        for(ArrayList<Process> a : all)
            for (Process p : a)
                if (p.PID == pid) {
                    p.priority = pr;
                    a.remove(p);
                    int pid1 = p.PID;
                    addProc(p.name, p.time, p.priority);
                    p.PID = pid1;
                    return;
                }
    }

    public void deleteProc(int pid) {
        for(ArrayList<Process> a : all)
            for (Process p : a)
                if (p.PID == pid) {
                    a.remove(p);
                    return;
                }
    }
}
