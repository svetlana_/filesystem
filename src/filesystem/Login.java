package filesystem;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

class UserData {
    short UID, GID;
    char[] username = new char[19];
    byte[] password;
    int r = 0, w = 0, x = 0;        //чтение, запись, удаление/создание/изменение
    UserData() {}
    UserData(short _UID, short _GID, char[] _username, byte[] _password) {
        UID = _UID;
        GID = _GID;
        username = _username;
        password = _password;
    }

    public char[] getUsername() {
        return username;
    }

    public void setRights(boolean _r, boolean _w, boolean _x) {
        r = (_r) ? 4 : 0;
        w = (_w) ? 2 : 0;
        x = (_x) ? 1 : 0;
    }

    public short getAccess() {
        return (short)(r+w+x);
    }
}

class Group {
    short GID;
    List<UserData>users;
    int r = 0, w = 0, x = 0;
    Group() {}
    public Group(short _GID) {
        GID = _GID;
        users = new ArrayList<>();
    }

    public void addUser(UserData usr) {
        this.users.add(usr);
    }

    public void setRights(boolean _r, boolean _w, boolean _x) {
        r = (_r) ? 4 : 0;
        w = (_w) ? 2 : 0;
        x = (_x) ? 1 : 0;
    }

    public void setRights(int access) {
        int[] acc = new int[3];
        int i = 0;
        while (access > 0) {        //получение прав для пользователей, групп и других
            int d = access / 10;
            int k = access - d * 10;
            access = d;
            acc[i] = k;
            i++;
        }

        i = 1;
        while (i < 2) {
            if (acc[i] == 7) setRights(true, true, true);
            else if (acc[i] == 5) setRights(true, false, true);
            else if (acc[i] == 6) setRights(true, true, false);
            else if (acc[i] == 4) setRights(true, false, false);
            else if (acc[i] == 3) setRights(false, true, true);
            else if (acc[i] == 2) setRights(false, true, false);
            else if (acc[i] == 1) setRights(false, false, true);
            else if (acc[i] == 0) setRights(false, false, false);
        }
    }

    public short getAccess() {
        return (short)(r+w+x);
    }
}

public class Login {
    List<UserData> users;
    List<Group> groups;
    List<UserData> others;
    public UserData other = new UserData((short)-1, (short)-1, "other".toCharArray(), "".getBytes());
    Login() {
        users = new ArrayList<>();
        groups = new ArrayList<>();
        others = new ArrayList<>();
    }

    public UserData findUser(String username){
        UserData u = null;
        for(UserData ud : users)
            if(new String(ud.username).trim().equals(username))
                u = ud;
        return u;
    }

    public List<String> getUsersList(){
        List<String>array = new ArrayList<>();
        for(UserData ud : users)
            array.add(new String(ud.getUsername()));
        return array;
    }

    public short getUID(String username) {
        UserData u = findUser(username);
        return (u == null) ? (short)setID() : u.UID;       //если пользователь с таким именем существует, тогда возвращается его UID, иначе - генерируется новый
    }

    public short getGID(String username) {
        UserData u = findUser(username);
        return (u == null) ? (short)setID() : u.GID;
    }

    private int setID() {
        Random r = new Random();
        return r.nextInt(100);
    }

    public short getAccessForInode(int u, int g) {
        StringBuilder sb = new StringBuilder();
        sb.append(u);
        sb.append(g);
        sb.append(5);       //по умолчанию у всех других пользователей доступ = r-x
        int res = 0;
        String s = sb.toString();
        res = Integer.parseInt(s);
        return (short)res;
    }

    public short checkAccess(Inode inode, UserData user) {
        int[] acc = new int[3];
        int i = 2;
        int access = (int)inode.access;
        while (access > 0) {        //получение прав для пользователей, групп и других
            int d = access / 10;
            int k = access - d * 10;
            access = d;
            acc[i] = k;
            i--;
        }
        if(user.UID == 0)
            return 7;
        else if(inode.UID == user.UID)
            return (short)acc[0];
        else if(inode.GID == user.GID)
            return (short)acc[1];
        else
            return (short)acc[2];
    }

    public Group findGroup(short gid) {
        Group g = null;
        for(Group gd : groups)
            if(gd.GID == gid)
                g = gd;
        return g;
    }
}
