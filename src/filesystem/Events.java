package filesystem;

import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

public class Events {
    static Scanner inp = new Scanner(System.in);
    public static Disk disk;
    private static FileSystem fileSystem;
    public static int toFormat = 0;
    static Login login = new Login();

    public static void cerr(String s) {
        System.out.println(s);
    }

    //загрузка существующей системы
    public static void boot() {
        disk = new Disk("NewDisk");
        fileSystem = new FileSystem();
        if(toFormat == 1)
            format();
        else {
            fileSystem.user = login();
            fileSystem.group = login.findGroup(fileSystem.user.GID);
        }
        fileSystem.login = login;
    }

    //форматирование системы
    public static void format(){
        fileSystem.formatDisk();
        fileSystem.formatWrite();
        fileSystem.user = new UserData((short)0, (short)0, "root".toCharArray(), "root".getBytes());
        fileSystem.user.setRights(true, true, true);        //права рута
        login.users.add(fileSystem.user);
        fileSystem.group = new Group((short)0);
        fileSystem.group.setRights(true, true, true);       //права группы админов
        login.groups.add(fileSystem.group);
        fileSystem.group.addUser(fileSystem.user);
        writeUserToFile(login.users.get(0), false);
    }

    private static void writeUserToFile(UserData u, boolean pr) {
        FileWriter writer = null;
        try {
            writer = new FileWriter("passwords.txt", pr);
            String data = Short.toString(u.UID) + ":" + Short.toString(u.GID) + ":" + new String(u.username) + ":" + hashPassword(new String (u.password));
            writer.write(data);
            writer.append(System.getProperty("line.separator"));
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String hashPassword(String p){
        /////////Хеширование введённого пароля для сравнения с хешем из файла/////////
        StringBuffer code = new StringBuffer(); //the hash code
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        byte bytes[] = p.getBytes();
        byte digest[] = messageDigest.digest(bytes); //create code
        for (int i = 0; i < digest.length; ++i) {
            code.append(Integer.toHexString(0x0100 + (digest[i] & 0x00FF)).substring(1));
        }
        return code.toString();
    }

    //Добавление нового пользователя
    private static void addUser(String username, String password) {
        for(Group gr : login.groups)
            System.out.println(gr.GID);
        System.out.println("Выберите ID группы:");
        String g = inp.nextLine();
        if(login.findGroup(Short.parseShort(g)) != null) {
            UserData newUser = new UserData(login.getUID(username), Short.parseShort(g), username.toCharArray(), password.getBytes());
            newUser.setRights(true, true, false);
            login.users.add(newUser);
            writeUserToFile(newUser, true);
        }
        else System.out.println("Группы с таким id не существует, хотите создать? y/n");
        String a = inp.nextLine();
        if(a.equals("y")) {
            addGroup();
            login.groups.get(login.groups.size()-1).GID = Short.parseShort(g);
        }
        else if(a.equals("n")) Events.cerr("Пользователь не был создан!");
    }

    private static void addGroup() {
        Group newGr = new Group(login.getGID(""));     //получение id новой группы
        newGr.setRights(true, true, false);
        login.groups.add(newGr);
        System.out.println("Создана группа");
    }

    public static UserData login() {
        UserData user = null;
        do {
            System.out.println("Имя пользователя:");
            String u = inp.nextLine();
            System.out.println("Пароль:");
            String p = inp.nextLine();
            String md5 = hashPassword(p);

            //////Считывание данных (имя, хеш, UID) из файла//////
            BufferedReader reader = null;
            String[] usersArray = null;
            try {
                reader = new BufferedReader(new FileReader("passwords.txt"));
                String users;
                List<String> lines = new ArrayList<String>();
                while ((users = reader.readLine()) != null) {
                    lines.add(users);
                }
                usersArray = lines.toArray(new String[lines.size()]);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            for (String s : usersArray) {
                String[] tokens = s.split("[:]+");
                UserData usr;
                usr = new UserData(Short.parseShort(tokens[0]), Short.parseShort(tokens[1]), tokens[2].toCharArray(), tokens[3].getBytes()); //передача UID, имени и пароля
                Group gr = new Group(Short.parseShort(tokens[1]));      //считывание существующей группы
                gr.addUser(usr);        //добавления пользователя в группу в ram
                login.users.add(usr);       //добавление в память каждого записанного в файл пользователя
                login.groups.add(gr);
                if (md5.equals(tokens[3])) {
                    user = usr;
                }
            }
            login.other.setRights(true, false, true);
        }while (user == null);
        return user;
    }

    public static void help() {
        System.out.println("format - Форматировать диск");
        System.out.println("pwd - Показать текущее местоположение (директорию)");
        System.out.println("cd - Перейти в директорию");
        System.out.println("cat - Создать файл в текущей директории");
        System.out.println("write - Записать в пустой файл");
        System.out.println("edit - Добавить к файлу");
        System.out.println("read - Прочесть содержимое файла");
        System.out.println("mkdir - Создать файл в текущей директории");
        System.out.println("ls - Показать содержимое текущей директории");
        System.out.println("rm -f - Удалить файл из текущей директории");
        System.out.println("rmdir - Удалить директорию");
        System.out.println("mv - Переместить файл/директорию");
        System.out.println("addusr - Добавить пользователя");
        System.out.println("addgr - Добавить группу");
        System.out.println("chmod - Изменить права доступа");
        System.out.println("pstart - Запустить планировщик");
        System.out.println("reboot - Сменить пользователя");
    }

    public static void displayMenu() {
        while(true) {
            System.out.println();
            System.out.println("help - Помощь по командам");

            String choice = inp.nextLine();
            switch (choice) {
                case "reboot":
                    fileSystem.user = login();
                    fileSystem.group = login.findGroup(fileSystem.user.GID);
                    break;
                case "help":
                    help();
                    break;
                case "chmod":
                    if(fileSystem.user.GID != 0) {    //если пользователь не принадлежит группе администраторов
                        Events.cerr("Недостаточно прав для совершения действия!");
                        return;
                    }
                    System.out.println("Введите полное имя файла/директории:");
                    String n = inp.nextLine();
                    if(fileSystem.isExist(n)) {
                        System.out.println("Введите новые права (трёхзначное число):");
                        String r = inp.nextLine();
                        short rr = (short) Integer.parseInt(r);
                        if (rr > 777) {
                            Events.cerr("Неверный ввод!");
                            return;
                        }
                        fileSystem.chmod(n, rr);
                    }
                    else Events.cerr("Неверное имя!");
                    break;
                case "mv":
                    System.out.println("Введите короткое имя файла/директории, которые хотите переместить из текущей директории:");
                    String shortName = inp.nextLine();
                    String fullNameSource = fileSystem.getCurDirName() + shortName;
                    if(fileSystem.isExist(fullNameSource)) {
                        System.out.println("Введите полное имя директории-назначения:");
                        String fullNameDestination = inp.nextLine();
                        if(fileSystem.isExist(fullNameDestination))
                            fileSystem.move(fullNameSource, fullNameDestination, shortName);
                        else Events.cerr("Неверное имя!");
                    }
                    else Events.cerr("Неверное имя!");

                    break;
                case "addusr":
                    System.out.println("Введите имя нового пользователя:");
                    String s0 = inp.nextLine();
                    System.out.println("Введите пароль:");
                    String ss = inp.nextLine();
                    addUser(s0, ss);
                    break;
                case "addgr":
                    addGroup();
                    break;
                case "format":
                    disk = new Disk("C://NewDisk1");
                    format();
                    break;
                case "pwd":
                    System.out.println(fileSystem.getCurDirName());
                    break;
                case "cd":
                    System.out.println("Введите путь к директории, в которую хотите перейти:");
                    String s1 = inp.nextLine();
                    if(s1.toCharArray()[0] != '/' || !(s1.endsWith("/")))
                        Events.cerr("Неверное имя файла");
                    else fileSystem.setCurDir(s1);
                    break;
                case "mkdir":
                    System.out.println("Введите имя директории, которую хотите создать:");
                    String s2 = inp.nextLine();
                    String fullName = fileSystem.getCurDirName() + s2 + "/";
                    if(!fileSystem.isExist(fullName)) {
                        fileSystem.createDataElement(fullName);
                    }
                    else Events.cerr("Директория с таким именем уже существует!");
                    break;
                case "cat":
                    System.out.println("Введите имя файла, который хотите создать:");
                    String s3 = inp.nextLine();
                    fullName = fileSystem.getCurDirName() + s3;
                    if(!fileSystem.isExist(fullName)) {
                        fileSystem.createDataElement(fullName);
                    }
                    else Events.cerr("Файл с таким именем уже существует!");
                    break;
                case "write":
                    System.out.println("Введите имя файла, в который хотите записать:");
                    String s4 = inp.nextLine();
                    fullName = fileSystem.getCurDirName() + s4;
                    if(fileSystem.isExist(fullName)) {
                        writeToFileMenu(fullName, false);
                        fileSystem.SBUpdate();
                    }
                    else Events.cerr("Файл не найден!");
                    break;
                case "edit":
                    System.out.println("Введите имя файла, в который хотите дописать:");
                    String s44 = inp.nextLine();
                    fullName = fileSystem.getCurDirName() + s44;
                    if(fileSystem.isExist(fullName)) {
                        writeToFileMenu(fullName, true);
                        fileSystem.SBUpdate();
                    }
                    else Events.cerr("Файл не найден!");
                    break;
                case "read":
                    System.out.println("Введите имя файла, в который хотите прочесть:");
                    String s5 = inp.nextLine();
                    fullName = fileSystem.getCurDirName() + s5;
                    if(fileSystem.isExist(fullName)) {
                        byte[] data = fileSystem.readFromFile(fullName);
                        if(data != null) System.out.println(new String(data));
                    }
                    else Events.cerr("Файл не найден!");
                    break;
                case "ls":
                    fileSystem.showDirContent(false);
                    break;
                case "ls -l":
                    fileSystem.showDirContent(true);
                    break;
                case "rm -f":
                case "rmdir":
                    System.out.println("Введите имя файла, который хотите удалить:");
                    String s6 = inp.nextLine();
                    fullName = fileSystem.getCurDirName() + s6;
                    if(fileSystem.isExist(fullName)) {
                        fileSystem.delete(fullName);
                        System.out.println("Файл удалён!");
                    }
                    else Events.cerr("Файл не найден!");
                    break;
                case "pstart":
                    schedule();
                    break;
                default:
                    System.out.println("Невозможно распознать команду:" + choice);
            }
        }
    }

    private static void writeToFileMenu(String fullName, boolean pr) {
        System.out.println("Как вы хотите ввести данные?");
        System.out.println("f - из файла");
        System.out.println("w - вручную");
        String s1 = inp.nextLine();
        int length;
        byte[] tmp = new byte[10240];
        switch (s1) {
            case "f":
                try {
                    System.out.println("Введите имя внешнего файла для чтения:");
                    String s2 = inp.nextLine();
                    File f = new File(s2);
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    InputStream in = new FileInputStream(f);
                    byte[] existdata = null;

                    if (pr) {    //если добавить к существующему
                        //existdata = new byte[10240];
                        existdata = fileSystem.readFromFile(fullName);
                        out.write(existdata);
                    }
                    while ((length = in.read(tmp)) >= 0)
                        out.write(tmp, 0, length);
                    fileSystem.writeToFile(fullName, out.toByteArray());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case "w":
                System.out.println("Для выхода введите с новой строки !q");
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                byte[] existdata = null;

                try {
                    if (pr) {    //если добавить к существующему
                    //existdata = new byte[10240];
                    existdata = fileSystem.readFromFile(fullName);
                    out.write(existdata);
                    }
                    String inpStr = "";
                    FileWriter writer = new FileWriter("data.txt", false);
                    do {
                        writer.write(inpStr);
                        writer.append(System.getProperty("line.separator"));
                        inpStr = inp.nextLine();
                    } while (!Objects.equals(inpStr, "!q"));
                    writer.flush();

                    InputStream in = new FileInputStream(new File("data.txt"));

                    while ((length = in.read(tmp)) >= 0)
                        out.write(tmp, 0, length);

                    fileSystem.writeToFile(fullName, out.toByteArray());
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            default:
                Events.cerr("Неверный ввод");
                return;
        }
    }

    private static void schedule() {
        Scheduler scheduler = new Scheduler();
        scheduler.init();
        scheduler.print();

        while(true) {
            System.out.println();
            System.out.println("s - Завершить текущий квант");
            System.out.println("renice - Изменить приоритет процесса");
            System.out.println("add - Добавить новый процесс");
            System.out.println("kill - Удалить процесс");

            String choice = inp.nextLine();
            switch (choice) {
                case "s":
                    scheduler.run();
                    scheduler.print();
                    break;
                case "renice":
                    System.out.println("Введите PID процесса, приоритет которого хотите изменить:");
                    int i1 = inp.nextInt();
                    System.out.println("Введите новый приоритет:");
                    int i2 = inp.nextInt();
                    if (i2 > 9 && i2 < 40)
                        scheduler.changePriority(i1, i2);
                    else Events.cerr("Указанный приоритет может быть задан только ядром или выходит за границы допустимого.");
                    scheduler.print();
                    break;
                case "add":
                    System.out.println("Введите имя нового эмулируемого процесса:");
                    String s1 = inp.nextLine();
                    System.out.println("Введите его приоритет:");
                    int i3 = inp.nextInt();
                    System.out.println("Введите время его выполнения:");
                    int i4 = inp.nextInt();
                    scheduler.addProc(s1, i4, i3);
                    scheduler.print();
                    break;
                case "kill":
                    System.out.println("Введите PID процесса, который хотите удалить:");
                    int i5 = inp.nextInt();
                    scheduler.deleteProc(i5);
                    scheduler.print();
                    break;
                default:
                    System.out.println("Невозможно распознать команду:" + choice);
            }
        }
    }
}
