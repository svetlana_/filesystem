package filesystem;

import com.sun.xml.internal.ws.util.ByteArrayBuffer;

import java.io.IOException;
import java.nio.ByteBuffer;

public class Ilist {
    public static Inode[] inodes;

    public Ilist(int num) throws IOException {
        inodes = new Inode[num];
        for (int i = 1; i < inodes.length; i++) {
            inodes[i] = new Inode(i);
        }
    }

    public byte[] getBytes() {
        byte[] rawData;
        ByteArrayBuffer buffer = new ByteArrayBuffer();
        try {
            for (Inode i : inodes) {
                buffer.write(i.getBytes());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        rawData = buffer.getRawData();
        System.out.println("Список инодов записан");
        return rawData;
    }




}