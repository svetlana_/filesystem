package filesystem;

import com.sun.xml.internal.ws.util.ByteArrayBuffer;
import java.nio.ByteBuffer;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Svetlana on 24.11.2016.
 */

public class BlockList {
    Block[] freeBlocks;       //список свободных/занятых блоков

    public BlockList(int size) throws IOException {
        freeBlocks = new Block[size];

        for (int i = 0; i < freeBlocks.length; i++) {
            freeBlocks[i] = new Block(i);
        }
    }

    public byte[] getBytes() throws IOException {
        byte[] rawData;
        ByteArrayBuffer buffer = new ByteArrayBuffer(10);
        for (Block b : freeBlocks) {
            buffer.write(b.getBytes());
        }

        rawData = buffer.getRawData();
        System.out.println("Список блоков записан");
        return rawData;
    }
}
